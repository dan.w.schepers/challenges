Please find the ManageTeam react components directory. This is a real example from the Bridge code repository. Please complete a thorough code review of these files, esp considering:
- Would you make any changes to the file naming or structure themselves? Why or why not?
  - In general, I like each component to have it's own folder: `Foo/`
    contains the files `Foo.jsx`, `FooSpec.jsx`, & `Foo.scss`. It makes for
a 1 stop shop when I need to know things about `Foo`. I also like to
include subfolders, see next point
  - I like to nest components in the manner they are used. For example,
    `ReassignPanel` is only used by `UserRow`, so `UserRow/` would have
a subfolder, `ReassignPanel`. When a component is used by more than one
consumer, it gets moved upward.
- Can you improve the way data is being passed between these components?
  - A quick thing to consider would be a state management tool, such as
    redux. There are props that are passed through components without
ever being used - for example, `UserRow` takes an `objectId` only so it
can pass it onto `ReassignPanel`. A state management tool could remove
the need for pass-through props, making components clearer in their
purpose (and easier to test).

For the purposes of this assignment, assume all json endpoints return exactly the data needed for each component. Please add comments in gitlab directly. Your role is to both improve the underlying code AND to explain why you're suggesting changes, as you would in a real code review.


* In order to allow commenting in GL, I added whitespace to the files to
  get them to show up in the diff. See [this open
issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/51823)
