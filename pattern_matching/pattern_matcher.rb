# frozen_string_literal: true

require 'active_support'
require 'active_support/core_ext'

class PatternMatcher
  attr_reader :pattern, :string

  def initialize(pattern, string)
    raise ArgumentError, 'pattern and input must be present' unless pattern.present? && string.present?

    @pattern = pattern.to_s
    @string = string.to_s
  end

  def matches?
    permutations.any? do |permutation|
      permutation.length == pattern.length &&
        permutation_fits_pattern?(permutation)
    end
  end

  private

  def permutation_fits_pattern?(permutation)
    identifiers = {}
    pattern.chars.zip(permutation) do |identifier, substring|
      identifiers[identifier] ||= substring
      return false unless identifiers[identifier] == substring
    end
    true
  end

  def permutations(result = [], left = [], remainder = string)
    result << left + Array(remainder)

    1.upto(remainder.length - 1).with_object(result) do |divider|
      permutations(result, left + Array(remainder[0..divider - 1]), remainder[divider..-1])
    end
  end
end
