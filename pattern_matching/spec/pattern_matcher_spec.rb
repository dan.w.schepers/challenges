# frozen_string_literal: true

require_relative '../pattern_matcher'

describe PatternMatcher do
  subject { described_class.new(pattern, string) }

  shared_examples 'a successful match' do
    it 'matches' do
      expect(subject.matches?).to be true
    end
  end

  shared_examples 'an unsuccessful match' do
    it 'matches' do
      expect(subject.matches?).to be false
    end
  end

  describe '#matches?' do
    context 'given invalid input' do
      it 'raises error when given no input' do
        expect { described_class.new('ab', '') }.to raise_error(ArgumentError)
      end

      it 'raises error when given no pattern' do
        expect { described_class.new('', 'redyellowblue') }.to raise_error(ArgumentError)
      end
    end

    context 'given valid input' do
      context 'given one part pattern' do
        context "'a' pattern" do
          let(:pattern) { 'a' } # anything should match this pattern
          let(:string) { 'red' }
          it_behaves_like 'a successful match'
        end

        context "'aaaa' pattern" do
          let(:pattern) { 'aaaa' }

          context 'the string matches the pattern' do
            let(:string) { 'redredredred' }
            it_behaves_like 'a successful match'
          end

          context 'the string does not match the pattern' do
            let(:string) { 'redblueredblue' }
            it_behaves_like 'an unsuccessful match'
          end
        end
      end

      context 'given multipart patterns' do
        context "'aba' pattern" do
          let(:pattern) { 'aba' }

          context 'the string matches' do
            let(:string) { 'redbluered' }
            it_behaves_like 'a successful match'
          end

          context 'the string does not repeat enough' do
            let(:string) { 'p' }
            it_behaves_like 'an unsuccessful match'
          end

          context 'the string has non matching characters' do
            let(:string) { 'pqpr' }
            it_behaves_like 'an unsuccessful match'
          end
        end

        context "'abba' pattern" do
          let(:pattern) { 'abba' }

          context 'the string matches' do
            let(:string) { 'redbluebluered' }
            it_behaves_like 'a successful match'
          end

          context 'the string does not repeat enough' do
            let(:string) { 'redbluered' }
            it_behaves_like 'an unsuccessful match'
          end

          context 'the string has non matching characters' do
            let(:string) { 'pqpr' }
            it_behaves_like 'an unsuccessful match'
          end
        end
      end
    end
  end
end
